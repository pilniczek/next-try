// https://www.freecodecamp.org/news/the-100-correct-way-to-do-css-breakpoints-88d6a5ba1862/#tip-1-get-your-breakpoints-right

import { type BreakpointsOptions } from "@mui/material/styles";

const breakpoints:BreakpointsOptions = {
	values: {
		xs: 360,
		sm: 600,
		md: 900,
		lg: 1200,
		xl: 1800,
	}
}

export default breakpoints