import { createTheme } from "@mui/material";

import sharedTheme from "./theme"

const theme = createTheme({
	palette: {
		mode: 'dark',
	},
	...sharedTheme
});

export default theme;
