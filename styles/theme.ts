import breakpoints from "./breakpoints"
import spacing from "./spacing";

const theme = {
	spacing,
	breakpoints,
	components: {
		MuiCssBaseline: {
			styleOverrides: {
				html: {
					overflowY: "scroll",
					// fontSize: "62.5%;", // preferred this to typography.pxToRem(), because typography.pxToRem() is poorly documented and therefore useless
				},
			},
		},
	},
};

export default theme;
