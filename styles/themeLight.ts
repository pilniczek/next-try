import { createTheme } from "@mui/material";

import { blue, green, orange, purple } from "./designColors";
import sharedTheme from "./theme"

const theme = createTheme({
	palette: {
		mode: 'light',
		primary: {
			light: orange["300"],
			main: orange["500"],
			dark: orange["700"],
		},
		secondary: {
			light: purple["100"],
			main: purple["300"],
			dark: purple["500"],
		},
		info: {
			light: blue["000"],
			main: blue["500"],
		},
		success: {
			light: green["000"],
			main: green["500"],
		},
		error: {
			light: orange["000"],
			main: orange["700"],
		},
		warning: {
			light: orange["000"],
			main: orange["300"],
		},
	},
	...sharedTheme
});

export default theme;
