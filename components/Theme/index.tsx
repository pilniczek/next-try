import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import React, { type ReactNode } from "react";

import { useColorMode } from "../../context/ColorMode";
import themeDark from "../../styles/themeDark";
import themeLight from "../../styles/themeLight";

const Theme = ({ children }: { children: ReactNode }): JSX.Element => {
	const { mode } = useColorMode();

	return (
		<ThemeProvider theme={mode === "dark" ? themeDark : themeLight}>
			<CssBaseline />
			{children}
		</ThemeProvider>
	);
};

export default Theme;
