import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import Box from "@mui/material/Box";
import React, { type ReactNode } from "react";

import Header from "../Header";

const Layout = ({ children }: { children: ReactNode }): JSX.Element => {
	return (
		<Box px={4} py={2}>
			<Header />
			{children}
		</Box>
	);
};

export default Layout;
