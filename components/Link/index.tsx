import MUILink from "@mui/material/Link";
import NextLink from "next/link";
import React, { forwardRef } from "react";

// const LinkBehavior = forwardRef((props, ref) => <NextLink ref={ref} {...props} />);

const LinkBehavior = forwardRef(function InternalLink(props, ref) {
	return <NextLink ref={ref} {...props} />;
});

const Link = ({ ...rest }): JSX.Element => <MUILink {...rest} component={LinkBehavior} />;

export default Link;
