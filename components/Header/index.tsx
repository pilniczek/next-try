import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";
import React from "react";

import { useColorMode } from "../../context/ColorMode";

const Header = (): JSX.Element => {
	const theme = useTheme();
	const { toggleColorMode } = useColorMode();
	return (
		<Box>
			{theme.palette.mode} mode
			<IconButton sx={{ ml: 1 }} onClick={toggleColorMode} color="inherit">
				{theme.palette.mode === "dark" ? <Brightness7Icon /> : <Brightness4Icon />}
			</IconButton>
		</Box>
	);
};

export default Header;
