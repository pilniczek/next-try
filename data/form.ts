export const currencies = [
	{
		value: "USD",
		label: "$"
	},
	{
		value: "EUR",
		label: "€"
	},
	{
		value: "BTC",
		label: "฿"
	},
	{
		value: "JPY",
		label: "¥"
	}
];

export const marks = [
	{
		value: 0,
		label: "0°C"
	},
	{
		value: 20,
		label: "20°C"
	},
	{
		value: 37,
		label: "37°C"
	},
	{
		value: 100,
		label: "100°C"
	}
];

export const valuetext = (value: number): string => `${value}°C`;

export const defaultValues = {
	textInput: undefined,
	textSelect: undefined,
	select: undefined,
	switch: false,
	radios: "male"
};
