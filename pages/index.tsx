import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Head from "next/head";
import React from "react";

import Layout from "../components/Layout";
import Link from "../components/Link";

const Home = (): JSX.Element => {
	return (
		<Layout>
			<Head>
				<title>Create Next App</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<main>
				<Typography variant="h1">
					Welcome to <Link href="https://nextjs.org">Next.js!</Link>
				</Typography>

				<Typography variant="body1">
					Get started by editing <code>pages/index.js</code>
				</Typography>

				<Link href="/link">Home</Link>

				<Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								<Typography variant="h3">Documentation &rarr;</Typography>
								<Typography variant="body1">
									Find in-depth information about Next.js features and API.
								</Typography>
							</CardContent>
							<CardActions>
								<Link href="https://nextjs.org/docs">Link</Link>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								<Typography variant="h3">Learn &rarr;</Typography>
								<Typography variant="body1">
									Learn about Next.js in an interactive course with quizzes!
								</Typography>
							</CardContent>
							<CardActions>
								<Link href="https://nextjs.org/learn">Link</Link>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								<Typography variant="h3">Examples &rarr;</Typography>
								<Typography variant="body1">
									Discover and deploy boilerplate example Next.js projects.
								</Typography>
							</CardContent>
							<CardActions>
								<Link href="https://github.com/vercel/next.js/tree/master/examples">Link</Link>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								<Typography variant="h3">Deploy &rarr;</Typography>
								<Typography variant="body1">
									Instantly deploy your Next.js site to a public URL with Vercel.
								</Typography>
							</CardContent>
							<CardActions>
								<Link href="https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app">
									Link
								</Link>
							</CardActions>
						</Card>
					</Grid>
				</Grid>
			</main>

			<footer>
				<Link
					href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
					target="_blank"
					rel="noopener noreferrer"
				>
					Powered by <img src="/vercel.svg" alt="Vercel" />
				</Link>
			</footer>
		</Layout>
	);
};

export default Home;
