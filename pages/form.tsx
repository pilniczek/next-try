import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormLabel from "@mui/material/FormLabel";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Paper from "@mui/material/Paper";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import Select from "@mui/material/Select";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Head from "next/head";
import React from "react";
import { Controller, useForm } from "react-hook-form";

import Layout from "../components/Layout";
import Link from "../components/Link";
import Switch from "../components/Switch";
import { currencies, defaultValues } from "../data/form";

const Form = (): JSX.Element => {
	const { control, handleSubmit } = useForm({ defaultValues });

	const onSubmit = (data) => {
		console.log(data);
	};

	return (
		<Layout>
			<Head>
				<title>Create Next App</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<main>
				<Stack spacing={5}>
					<Typography variant="h1">Form</Typography>

					<Box>
						<Link href="/link">Home</Link>
					</Box>
					<Paper elevation={3}>
						<form onSubmit={handleSubmit(onSubmit)}>
							<Stack spacing={5} px={5} py={6}>
								<Controller
									name="textInput"
									control={control}
									render={({ field }) => (
										<TextField label="Outlined" variant="outlined" {...field} />
									)}
								/>

								<Controller
									name="textSelect"
									control={control}
									render={({ field }) => (
										<TextField
											id="outlined-select-currency"
											select
											label="Select"
											defaultValue=""
											helperText="Please select your currency"
											{...field}
										>
											{currencies.map((option) => (
												<MenuItem key={option.value} value={option.value}>
													{option.label}
												</MenuItem>
											))}
										</TextField>
									)}
								/>

								<Controller
									name="select"
									control={control}
									render={({ field }) => (
										<FormControl>
											<InputLabel id="demo-simple-select-label">Select</InputLabel>
											<Select {...field} labelId="demo-simple-select-label" label="Select">
												{currencies.map((option) => (
													<MenuItem key={option.value} value={option.value}>
														{option.label}
													</MenuItem>
												))}
											</Select>
										</FormControl>
									)}
								/>

								<Controller
									name="switch"
									control={control}
									render={({ field }) => (
										<FormControlLabel
											control={<Switch defaultChecked={defaultValues.switch} />}
											label="Wanna this?"
											{...field}
										/>
									)}
								/>

								<Controller
									name="radios"
									control={control}
									render={({ field }) => (
										<FormControl>
											<FormLabel id="demo-radio-buttons-group-label">Gender</FormLabel>
											<RadioGroup
												aria-labelledby="demo-radio-buttons-group-label"
												defaultValue={defaultValues.radios}
												{...field}
											>
												<FormControlLabel value="female" control={<Radio />} label="Female" />
												<FormControlLabel value="male" control={<Radio />} label="Male" />
												<FormControlLabel value="other" control={<Radio />} label="Other" />
											</RadioGroup>
										</FormControl>
									)}
								/>

								<Button variant="contained" type="submit">
									Submit
								</Button>
							</Stack>
						</form>
					</Paper>
				</Stack>
			</main>
		</Layout>
	);
};

export default Form;
