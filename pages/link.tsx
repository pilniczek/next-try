import Typography from "@mui/material/Typography";
import Head from "next/head";
import React from "react";

import Layout from "../components/Layout";
import A from "../components/Link";

const Link = (): JSX.Element => {
	return (
		<Layout>
			<Head>
				<title>Create Next App</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<main>
				<Typography variant="h1">
					Welcome to <A href="https://nextjs.org">Next.js!</A>
				</Typography>

				<A href="/">Home</A>
			</main>
		</Layout>
	);
};
export default Link;
