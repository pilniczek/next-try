import type { AppProps } from "next/app";
import React from "react";

import Theme from "../components/Theme";
import ColorModeProvider from "../context/ColorMode";

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
	return (
		<ColorModeProvider>
			<Theme>
				<Component {...pageProps} />
			</Theme>
		</ColorModeProvider>
	);
};
export default App;
