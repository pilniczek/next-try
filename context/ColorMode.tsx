import React, { createContext, type ReactNode, useContext, useState } from "react";

interface Mode {
	toggleColorMode: () => void;
	mode: "light" | "dark";
}

const ColorModeContext = createContext<Mode>({
	toggleColorMode: () => {},
	mode: "light"
});

export const useColorMode = (): Mode => useContext(ColorModeContext);

const ColorModeProvider = ({ children }: { children: ReactNode }): JSX.Element => {
	const [mode, setMode] = useState<"light" | "dark">("light");
	const toggleColorMode = (): void => {
		setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
	};
	return (
		<ColorModeContext.Provider
			value={{
				toggleColorMode,
				mode
			}}
		>
			{children}
		</ColorModeContext.Provider>
	);
};

export default ColorModeProvider;
